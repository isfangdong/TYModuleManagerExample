//
//  TYMainViewControllerProtocol.h
//  Pods
//
//  Created by 房栋 on 2018/3/29.
//

#import <Foundation/Foundation.h>

@protocol TYMainViewControllerProtocol <NSObject>

- (UIViewController * _Nonnull)mainViewController;

- (UIViewController * _Nonnull)loginViewController;

@end
